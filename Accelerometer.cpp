#include "Accelerometer.h"

void beginAccelerometer()
{
    Wire.begin();
    Wire.beginTransmission(ACCADRR);

    Wire.write(0x07); // Select mode register
    Wire.write(0x01); // Select active mode
    Wire.endTransmission(); // Stop I2C Transmission

    Wire.beginTransmission(ACCADRR);
    Wire.write(0x08); // Select sample rate register register
    Wire.write(0x07);   // 1 sample per second
    Wire.endTransmission();   // Stop I2C Transmission
}

void readAcceleration(float &ax, float &ay, float &az)
{
    unsigned int data[3];
    Wire.beginTransmission(ACCADRR);
    Wire.write(0x00); // Select Data Register
    Wire.endTransmission(); // Stop I2C Transmission

    Wire.requestFrom(ACCADRR, 3); // Request 3 bytes of data

    // Read the three bytes
    if (Wire.available() == 3)
    {
        data[0] = Wire.read();
        data[1] = Wire.read();
        data[2] = Wire.read();
    }

    int a_x = data[0] & 0x3F;
    if (a_x > 31)
        a_x -= 64;
    int a_y = data[1] & 0x3F;
    if (a_y > 31)
        a_y -= 64;
    int a_z = data[2] & 0x3F;
    if (a_z > 31)
        a_z -= 64;

    ax = (float)a_x / (float)21.;
    ay = (float)a_y / (float)21.;
    az = (float)a_z / (float)21.;
}
