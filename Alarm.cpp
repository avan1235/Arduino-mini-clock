#include "Alarm.h"

void beepAlarm() {
    if ((digitalRead(BUTTON_1) == HIGH || digitalRead(BUTTON_2) == HIGH)) {
        alarmBeep = false;
        alarmButtonClicked = true;
    }

    if (alarmBeep == true) {

        Display.firstPage();
        do {
            heartAnimation(0, 0, rHeart);
        } while (Display.nextPage());

        if ((unsigned long)millis() - lastLEDs >= LEDS_CHANGE_MILLIS) {
            lastLEDs = millis();

            if (LEDsState == 0 || LEDsState > 15)
                LEDsState = 1;
            else if (LEDsState == 1)
                LEDsState = 8;
            else if (LEDsState == 8)
                LEDsState = 2;
            else if (LEDsState == 2)
                LEDsState = 4;
            else if (LEDsState == 4)
                LEDsState = 1;
        }

        if ((unsigned long)millis() - lastBeep >= BEEP_TIME_MILLIS) {
            lastBeep = millis();

            if (buzzerState == true) {
                tone(BUZZER_PIN, BUZZER_TONE);
                buzzerState = false;
            }
            else {
                noTone(BUZZER_PIN);
                buzzerState = true;
            }
        }
    }
}

void checkBothButtons() {
    if (digitalRead(BUTTON_1) == HIGH && digitalRead(BUTTON_2) == HIGH) {
        if ((unsigned long)millis() - lastTwoButtonPressed >= BUTTONS_WAIT_MILLIS) {
            settingAlarm = !settingAlarm;
            lastTwoButtonPressed = millis();
        }
    }
    else {
        lastTwoButtonPressed = millis();
    }
}

void rearmAlarmBeep() {
    if ((actTime.hour == alarmHour && (actTime.minute + 1) == alarmMinute && alarmMinute > 0) || (actTime.minute == 59 && alarmMinute == 0 && (actTime.hour + 1) % 24 == alarmHour)) {
        alarmButtonClicked = false; // Alarm rearm one minute before beep
    }
    buzzerState = true; // always turn buzzer off when it's not alarm time and prepare it for the next alarm
    noTone(BUZZER_PIN);
    LEDsState = DEFAULT_LEDS_STATE;
}

void checkButton1() {
    if (digitalRead(BUTTON_1) == HIGH && digitalRead(BUTTON_2) == LOW) {
        if ((unsigned long)millis() - lastButton1Pressed >= BUTTONS_CHANGE_MILLIS) {
            setTime = !setTime;
            lastButton1Pressed = millis();
        }
    }
    else {
        lastButton1Pressed = millis();
    }
}

void checkButton2() {
    if (digitalRead(BUTTON_1) == LOW && digitalRead(BUTTON_2) == HIGH) {
        if ((unsigned long)millis() - lastButton2Pressed >= BUTTONS_CHANGE_MILLIS) {
            if (setTime == false) {
                alarmON = !alarmON;
            }
            else {
                if (alarmMinute < 59) {
                    alarmMinute++;
                }
                else {
                    alarmMinute = 0;
                    alarmHour++;
                }
                alarmMinute %= 60;
                alarmHour %= 24;
            }
            lastButton2Pressed = millis();
        }
    }
    else {
        lastButton2Pressed = millis();
    }
}

bool isAlarmTime() {
    return (alarmON == true && actTime.hour == alarmHour && actTime.minute == alarmMinute && alarmButtonClicked == false);
}

void useLEDs(byte shift) {
    shift %= 16;

    if ((byte)(shift & (byte)1) > 0)
        digitalWrite(LED_1, HIGH);
    else
        digitalWrite(LED_1, LOW);

    if ((byte)(shift & (byte)2) > 0)
        digitalWrite(LED_2, HIGH);
    else
        digitalWrite(LED_2, LOW);

    if ((byte)(shift & (byte)4) > 0)
        digitalWrite(LED_3, HIGH);
    else
        digitalWrite(LED_3, LOW);

    if ((byte)(shift & (byte)8) > 0)
        digitalWrite(LED_4, HIGH);
    else
        digitalWrite(LED_4, LOW);
}
