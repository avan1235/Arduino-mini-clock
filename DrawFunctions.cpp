#include "DrawFunctions.h"

void drawText(String toPrint, byte  x, byte y, byte font) {
    switch (font) {
        case 1:
            Display.setFont(u8g_font_4x6);
            break;
        case 2:
            Display.setFont(u8g_font_6x13);
            break;
        case 3:
            Display.setFont(u8g_font_courB10r);
            break;
    }

    Display.drawStr(x, y, toPrint.c_str());
}

double angleConvert(byte value, byte div) {
    return ((((double)90) - (((double)(value) / (double)div * (double)360))) / (double)360 * (double)2 * (double)M_PI);
}

void drawCirClock(byte vecX, byte vecY, byte radius) {
    double secondAngle = angleConvert(actTime.second, 60);
    double minuteAngle = angleConvert(actTime.minute, 60);
    double hourAngle = (((double)90) - (((double)(actTime.hour % 12) / (double)12 * (double)360) + ((double)actTime.minute / (double)720 * (double)360))) / (double)360 * (double)2 * (double)M_PI;
    double lineAngles;

    Display.drawCircle(XSize / 2 + vecX, YSize / 2 + vecY, radius);
    Display.drawDisc(XSize / 2 + vecX, YSize / 2 + vecY, 2);

    Display.drawLine(XSize / 2 + vecX, YSize / 2 + vecY, XSize / 2 + vecX + ((radius / 2)*cos(hourAngle)), YSize / 2 + vecY - ((radius / 2)*sin(hourAngle)));

    Display.drawLine(XSize / 2 + vecX, YSize / 2 + vecY, XSize / 2 + vecX + ((radius - 5)*cos(minuteAngle)), YSize / 2 + vecY - ((radius - 5)*sin(minuteAngle)));
    Display.drawLine(XSize / 2 + vecX, YSize / 2 + vecY, XSize / 2 + vecX + ((radius - 2)*cos(secondAngle)), YSize / 2 + vecY - ((radius - 2)*sin(secondAngle)));

    for (byte i = 0; i < 12; i++) {
        lineAngles = angleConvert(i, 12);
        Display.drawLine(XSize / 2 + vecX + ((radius)*cos(lineAngles)), YSize / 2 + vecY - ((radius)*sin(lineAngles)), XSize / 2 + vecX + ((radius - ((i % 3 == 0) ? 4 : 3))*cos(lineAngles)), YSize / 2 + vecY - ((radius - ((i % 3 == 0) ? 4 : 3))*sin(lineAngles)));
    }

    drawText("12", XSize / 2 + vecX + ((radius) * 0) - 4, YSize / 2 + vecY - ((radius) * 1) + 11, SMALL_FONT);
    drawText("6", XSize / 2 + vecX + ((radius) * 0) - 2, YSize / 2 + vecY - ((radius) * (-1)) - 5, SMALL_FONT);
    drawText("3", XSize / 2 + vecX + ((radius) * 1) - 8, YSize / 2 + vecY - ((radius) * 0) + 3, SMALL_FONT);
    drawText("9", XSize / 2 + vecX + ((radius) * (-1)) + 6, YSize / 2 + vecY - ((radius) * 0) + 3, SMALL_FONT);
}

void drawTemperature(byte x, byte y, byte font) {
    drawText("Temp.:", x, y, font);
    drawText(String(actTemp, 0) + (String)(char)'°' + "C", x, y + 15, font);
}

void drawHumidity(byte x, byte y, byte font) {
    drawText("Wilg.:", x, y, font);
    drawText(String(actHum, 0) + (String)(char)'%', x, y + 15, font);
}

void drawTime(byte x, byte y, byte font, bool seconds) {
    drawText(getStringTime(seconds), x, y, font);
}

void drawModuleOfNumber(byte XVec, byte YVec, bool state) {
    if (state) {
        for (byte i = 0; i < NUMBER_BOLD; i++) {
            Display.drawLine(XSize / 2 + XVec + abs(NUMBER_BOLD / 2 - i), YSize / 2 + YVec + i, XSize / 2 + XVec + NUMBER_SIZE - abs(NUMBER_BOLD / 2 - i), YSize / 2 + YVec + i);
        }
    }
    else {
        for (byte i = 0; i < NUMBER_BOLD; i++) {
            Display.drawLine(XSize / 2 + XVec + i, YSize / 2 + YVec + abs(NUMBER_BOLD / 2 - i), XSize / 2 + XVec + i, YSize / 2 + YVec + NUMBER_SIZE - abs(NUMBER_BOLD / 2 - i));
        }
    }
}

/*  How number works?
    Built with 7 modules
  1
*******
*2    *3
*  4  *
*******
*5    *6
*  7  *
*******
*/

void drawPartOfNumber(byte XVec, byte YVec, byte whichPart) {
    if ((byte)(whichPart & (byte)1) > 0)
        drawModuleOfNumber(XVec + 2, YVec - 2, HORIZONTAL);
    if ((byte)(whichPart & (byte)2) > 0)
        drawModuleOfNumber(XVec - 2, YVec + 2, VERTICAL);
    if ((byte)(whichPart & (byte)4) > 0)
        drawModuleOfNumber(XVec + NUMBER_SIZE, YVec + 2, VERTICAL);
    if ((byte)(whichPart & (byte)8) > 0)
        drawModuleOfNumber(XVec + 2, YVec + NUMBER_SIZE, HORIZONTAL);
    if ((byte)(whichPart & (byte)16) > 0)
        drawModuleOfNumber(XVec - 2, YVec + NUMBER_SIZE + 4, VERTICAL);
    if ((byte)(whichPart & (byte)32) > 0)
        drawModuleOfNumber(XVec + NUMBER_SIZE, YVec + NUMBER_SIZE + 4, VERTICAL);
    if ((byte)(whichPart & (byte)64) > 0)
        drawModuleOfNumber(XVec + 2, YVec + 2 * NUMBER_SIZE + 2, HORIZONTAL);
}

void drawRectangle(byte XVec, byte YVec, byte Size) {
    for (byte i = 0; i <= Size; i++) {
        Display.drawLine(XSize / 2 + XVec + ((i == 0 || i == Size ? 1 : 0)), YSize / 2 + YVec + i, XSize / 2 + XVec + Size - ((i == 0 || i == Size ? 1 : 0)), YSize / 2 + YVec + i);
    }
}

void drawNumber(byte number, byte positionOfDigit, byte XVec, byte YVec) {
    int valueToSendToFunctionDrawing;

    switch (number)
    {
        case 0: {
            valueToSendToFunctionDrawing = ZERO;
        } break;
        case 1: {
            valueToSendToFunctionDrawing = ONE;
        } break;
        case 2: {
            valueToSendToFunctionDrawing = TWO;
        } break;
        case 3: {
            valueToSendToFunctionDrawing = THREE;
        } break;
        case 4: {
            valueToSendToFunctionDrawing = FOUR;
        } break;
        case 5: {
            valueToSendToFunctionDrawing = FIVE;
        } break;
        case 6: {
            valueToSendToFunctionDrawing = SIX;
        } break;
        case 7: {
            valueToSendToFunctionDrawing = SEVEN;
        }break;
        case 8: {
            valueToSendToFunctionDrawing = EIGHT;
        } break;
        case 9: {
            valueToSendToFunctionDrawing = NINE;
        } break;
    }

    if (positionOfDigit == 1) {
        drawPartOfNumber(-62 + XVec, -26 + YVec, valueToSendToFunctionDrawing);
    }
    else if (positionOfDigit == 2) {
        drawPartOfNumber(-32 + XVec, -26 + YVec, valueToSendToFunctionDrawing);

    }
    else if (positionOfDigit == 3) {
        drawPartOfNumber(8 + XVec, -26 + YVec, valueToSendToFunctionDrawing);
    }
    else if (positionOfDigit == 4) {
        drawPartOfNumber(38 + XVec, -26 + YVec, valueToSendToFunctionDrawing);
    }
}

void drawDots(byte XVec, byte YVec) {
    drawRectangle(-4 + XVec, YVec, DOT_SIZE);
    drawRectangle(-4 + XVec, -16 + YVec, DOT_SIZE);
}

void drawBigTime(byte XVec, byte YVec) {
    drawDots(XVec, YVec);
    drawNumber(actTime.hour / 10, 1, XVec, YVec);
    drawNumber(actTime.hour % 10, 2, XVec, YVec);
    drawNumber(actTime.minute / 10, 3, XVec, YVec);
    drawNumber(actTime.minute % 10, 4, XVec, YVec);
}

void drawBase(byte XVec, byte YVec) {
    Display.drawDisc(XSize / 2 + XVec, YSize / 2 + YVec, THERMOMETER_SIZE);
    Display.drawLine(XSize / 2 + XVec - THERMOMETER_SIZE / 2 - 1, YSize / 2 + YVec, XSize / 2 + XVec - THERMOMETER_SIZE / 2 - 1, YSize / 2 + YVec - THERMOMETER_SIZE * 8);
    Display.drawLine(XSize / 2 + XVec + THERMOMETER_SIZE / 2 + 1, YSize / 2 + YVec, XSize / 2 + XVec + THERMOMETER_SIZE / 2 + 1, YSize / 2 + YVec - THERMOMETER_SIZE * 8);
    // Upper part of the thermometer
    for (int i = 0; i < THERMOMETER_SIZE / 2; i++) {
        Display.drawPixel(XSize / 2 + XVec + THERMOMETER_SIZE / 2 - i, YSize / 2 + YVec - THERMOMETER_SIZE * 8 - 1 - i);
        Display.drawPixel(XSize / 2 + XVec - THERMOMETER_SIZE / 2 + i, YSize / 2 + YVec - THERMOMETER_SIZE * 8 - 1 - i);
    }
    Display.drawPixel(XSize / 2 + XVec, YSize / 2 + YVec - THERMOMETER_SIZE * 8 - THERMOMETER_SIZE / 2);
}

void drawTermometer(byte XVec, byte YVec) {
    drawBase(XVec, YVec);
    byte drawTemp = actTemp;
    if (drawTemp >= MAX_TEMP) {
        drawTemp = MAX_TEMP;
    }
    if (drawTemp >= MIN_TEMP) {
        for (int i = 0; i <= THERMOMETER_SIZE / 2 + 1; i++) {
            Display.drawLine(XSize / 2 + XVec - THERMOMETER_SIZE / 2 - 1 + i, YSize / 2 + YVec, XSize / 2 + XVec - THERMOMETER_SIZE / 2 - 1 + i, YSize / 2 + YVec - (int)(((double)drawTemp - (double)MIN_TEMP) / ((double)MAX_TEMP - (double)MIN_TEMP) * (double)THERMOMETER_SIZE * (double)8));
            Display.drawLine(XSize / 2 + XVec + THERMOMETER_SIZE / 2 + 1 - i, YSize / 2 + YVec, XSize / 2 + XVec + THERMOMETER_SIZE / 2 + 1 - i, YSize / 2 + YVec - (int)(((double)drawTemp - (double)MIN_TEMP) / ((double)MAX_TEMP - (double)MIN_TEMP) * (double)THERMOMETER_SIZE * (double)8));
        }
    }
}

void drawHumidityPedometer() {
    Display.drawCircle(XSize / 2, YSize, XSize / 2 - 2);
    Display.drawCircle(XSize / 2, YSize, XSize / 2 - 1);
    Display.drawDisc(XSize / 2, YSize, 4);
    double angle = (((double)180) - (((double)(actHum) / (double)100 * (double)180))) / (double)360 * (double)2 * (double)M_PI;
    Display.drawLine(XSize / 2, YSize, XSize / 2 + ((XSize / 2 - 5)*cos(angle)), YSize - ((XSize / 2 - 5)*sin(angle)));
    Display.drawLine(XSize / 2 + 1, YSize, XSize / 2 + ((XSize / 2 - 5)*cos(angle)), YSize - ((XSize / 2 - 5)*sin(angle)));
    Display.drawLine(XSize / 2 - 1, YSize, XSize / 2 + ((XSize / 2 - 5)*cos(angle)), YSize - ((XSize / 2 - 5)*sin(angle)));
    drawText("0%", 5, YSize - 1, SMALL_FONT);
    drawText("100%", XSize - 18, YSize - 1, SMALL_FONT);
    drawText("50%", XSize / 2 - 4, YSize - XSize / 2 + 10, SMALL_FONT);
}

void drawSpecialDate(byte XVec, byte YVec, byte font) {
    drawText(getStringDayAndMonth(), XSize / 2 + XVec, YSize / 2 + YVec, font);
    drawText(getStringYear(), XSize / 2 + XVec + 5, YSize / 2 + YVec + 14, font);
}

void drawHeart(byte XVec, byte YVec, byte R) {
    Display.drawDisc(R + XVec, 0 + YVec, R);
    Display.drawDisc(-R + XVec, 0 + YVec, R);

    for (int x = 0; x < floor(R * (1 + 1 / (sqrt(2)))); x++) {
        Display.drawLine(x + XVec, -(x - R * (1 + sqrt(2))) + YVec, (-1)*x + XVec, -(x - R * (1 + sqrt(2))) + YVec);
    }

    byte x_max = floor(R * (1 + 1 / (sqrt(2))));

    for (int y = 0; y >= floor(-R / sqrt(2)); y--) {
        Display.drawLine(-x_max + 1 + XVec, -y + YVec, x_max - 1 + XVec, -y + YVec);
    }
}

void heartAnimation(byte XVec, byte YVec, byte actR) {
    drawHeart(XSize / 2, YSize / 2 - YSize / 11, actR);

    if ((unsigned long)millis() - lastHeart >= HEART_DELAY) {
        lastHeart = millis();
        if (heartIncrease) {
            rHeart = actR + 1;
        }
        else {
            rHeart = actR - 1;
        }
    }
    else {
        if (actR >= MAX_R_HEART) {
            heartIncrease = 0;
        }
        else if (actR <= MIN_R_HEART) {
            heartIncrease = 1;
        }
        rHeart = actR;
    }

}

/*
 * Calendar should be used only in landscape mode
*/

void drawDaysInCalendar(RTCDateTime time, byte XVec, byte YVec) {
    byte dayToCheck = time.day;
    byte monthToCheck = time.month;
    byte yearToCheck = time.year;

    byte passDay = dayOfWeek(yearToCheck, monthToCheck, 1) + 1;

    Display.setColorIndex(WHITE);

    for (byte i = 0; i < clockToRead.daysInMonth(yearToCheck, monthToCheck); i++)
    {
        drawText((String)(i + 1), XVec + 3 + ((i + passDay) % 7)*BOX_SIZE_X + 1 + (i < 9 ? 4 : 0), YVec + ((i + passDay) / 7)*BOX_SIZE_Y + BOX_SIZE_Y, SMALL_FONT);
    }

    Display.drawBox(XVec + (((dayToCheck + passDay) - 1) % 7)*BOX_SIZE_X, YVec + (((dayToCheck + passDay) - 1) / 7)*BOX_SIZE_Y + 2, BOX_SIZE_X - 2, BOX_SIZE_Y);
    Display.setColorIndex(BLACK);

    drawText((String)((dayToCheck)), XVec + 3 + (((dayToCheck + passDay) - 1) % 7)*BOX_SIZE_X + 1 + (dayToCheck < 9 ? 4 : 0), YVec + (((dayToCheck + passDay) - 1) / 7)*BOX_SIZE_Y + BOX_SIZE_Y, SMALL_FONT);
    Display.setColorIndex(WHITE);
}

void drawCalendar(RTCDateTime time, byte XVec, byte YVec) {
    Display.setColorIndex(WHITE);
    Display.drawBox(XVec, YVec, XSize, BOX_SIZE_Y + 1);
    Display.setColorIndex(BLACK);

    for (byte i = 0; i < 7; i++) {
        drawText(daysNames[i], i*BOX_SIZE_X + ((daysNames[i].length()) == 2 ? 8 : 5) + XVec, BOX_SIZE_Y - 1 + YVec, SMALL_FONT);
    }

    drawDaysInCalendar(time, XVec + 3, BOX_SIZE_Y + YVec);
}

void drawAlarmSetting() {
    Display.firstPage();
    do {
        drawText("ALARM", XSize / 2 - 20, YSize / 2 - 15, BOLD_FONT);
        if (setTime == false) {
            Display.drawBox(0, ((actScreenMode == -2 || actScreenMode == 2) ? (YSize / 2 - 7) : (YSize / 2 - 7)), XSize, ((actScreenMode == -2 || actScreenMode == 2) ? (14) : (29)));
            Display.setFontMode(BLACK);
            Display.setDrawColor(BLACK);
        }
        drawText("STAN:", ((actScreenMode == -2 || actScreenMode == 2) ? (XSize / 2 - 32) : (XSize / 2 - 17)), YSize / 2 + 5, BOLD_FONT);
        drawText(getAlarmState(), ((actScreenMode == -2 || actScreenMode == 2) ? (XSize / 2 + 15) : (XSize / 2 - 12)), ((actScreenMode == -2 || actScreenMode == 2) ? (YSize / 2 + 5) : (YSize / 2 + 20)), BOLD_FONT);
        if (setTime == true) {
            Display.drawBox(0, ((actScreenMode == -2 || actScreenMode == 2) ? (YSize / 2 + 7) : (YSize / 2 + 22)), XSize, 16);
            Display.setFontMode(BLACK);
            Display.setDrawColor(BLACK);
        }
        else {
            Display.setDrawColor(WHITE);
        }
        drawText(getAlarmTime(), XSize / 2 - 20, ((actScreenMode == -2 || actScreenMode == 2) ? (YSize / 2 + 20) : (YSize / 2 + 35)), BOLD_FONT);
        if (setTime == true) {
            Display.setDrawColor(WHITE);
        }
    } while (Display.nextPage());
}

void drawNormalScreens() {

    Display.firstPage();
    do {
        switch (actScreenMode) {
            case -1: {
                drawCirClock(0, -20, CLOCK_RADIUS);
                drawSpecialDate(-23, 40, BOLD_FONT);
            } break;
            case 1: {
                drawTermometer(-20, 20);
                drawTemperature(XSize / 2 - 4, 38, NORMAL_FONT);
                drawHumidity(XSize / 2 - 4, 71, NORMAL_FONT);
                drawHumidityPedometer();
                drawTime(XSize / 2 - 20, 15, BOLD_FONT, NO_SECONDS);
            } break;
            case -2: {
                drawBigTime(0, 0);
            } break;
            case 2: {
                drawCalendar(actTime, 0, 0);
            } break;
        }
    } while (Display.nextPage());

}