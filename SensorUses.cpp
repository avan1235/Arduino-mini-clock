#include "SensorUses.h"

/*
 * Acceleration sensor simple library
*/

void checkScreenRotation() {
    readAcceleration(ax, ay, az);
    byte direct = X_ROTATE; // 1-X, 2-Y, 3-Z
    float max_a = abs(ax);

    if (abs(ay) > max_a) {
        direct = Y_ROTATE;
        max_a = abs(ay);
    }
    if (abs(az) > max_a) {
        direct = Z_ROTATE;
        max_a = abs(az);
    }

    // Change depends on the orientation of sensor

    switch (direct) {
        case X_ROTATE: {
            if (ax < 0) {
                Display.setDisplayRotation(U8G2_R0);
                actScreenMode = -2;
            }
            else {
                Display.setDisplayRotation(U8G2_R2);
                actScreenMode = 2;
            }
        } break;
        case Y_ROTATE: {
            if (ay < 0) {
                Display.setDisplayRotation(U8G2_R3);
                actScreenMode = 1;
            }
            else {
                Display.setDisplayRotation(U8G2_R1);
                actScreenMode = -1;
            }
        } break;
        case Z_ROTATE: {
            alarmBeep = false;	// do not change rotation but disable alarm beep
            alarmButtonClicked = true;
        } break;
    }
}

void actualizeScreenModeSize() {
    switch (abs(actScreenMode)) {
        case 1: {
            XSize = XSizeV;
            YSize = YSizeV;
        } break;
        case 2: {
            XSize = XSizeH;
            YSize = YSizeH;
        } break;
    }
}

/*
 * Clock sensor
*/

void actualizeTime() {
    actTime = clockToRead.getDateTime();
}

byte dayOfWeek(uint16_t year, uint8_t month, uint8_t day)
{
    uint16_t months[] =
            {
                    0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365 // days until 1st of month
            };

    uint32_t days = year * 365;   // days until year
    for (uint16_t i = 4; i < year; i += 4)
    {
        if (LEAP_YEAR(i)) days++;  // adjust leap years
    }

    days += months[month - 1] + day;
    if ((month > 2) && LEAP_YEAR(year)) days++;
    return days % 7;
}

/*
 * Temperature sensor
*/

void checkTempAndHum() {
    if ((unsigned long)millis() - lastDHT >= DHT_DELAY_MILLIS) {
        check = DHT.read(DHT_PIN);
        actTemp = DHT.temperature;
        actHum = DHT.humidity;
        lastDHT = (unsigned long)millis();
    }
}
