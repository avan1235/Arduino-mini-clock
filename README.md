# Arduino Mini Clock

Arduino project to create own desk clock with acceleration sensor to detect the position of clock and basic weather informations like temperature and humidity live displayed on the screen.

### Prerequirements

The written code depends on Arduino libraries

1.  DS3231 library from https://github.com/NorthernWidget/DS3231
2.  U8g2_Arduino library from https://github.com/olikraus/U8g2_Arduino
3.  dht11 library from https://github.com/adidax/dht11

### Installing

Arduino IDE can be used to upload the sketch to the microcontroller. The new configuration of ATMEGA328P 8Mhz has to be added to boards.txt file as follows to program the microcontroller without the external clock (if needed)

```
AVR_ATMEGA328BB.name=ATmega328 on a breadboard (8 MHz internal clock)

AVR_ATMEGA328BB.upload.protocol=arduino
AVR_ATMEGA328BB.upload.maximum_size=30720
AVR_ATMEGA328BB.upload.speed=57600

AVR_ATMEGA328BB.bootloader.low_fuses=0xE2
AVR_ATMEGA328BB.bootloader.high_fuses=0xDA
AVR_ATMEGA328BB.bootloader.extended_fuses=0x05

AVR_ATMEGA328BB.bootloader.file=atmega/ATmegaBOOT_168_atmega328_pro_8MHz.hex
AVR_ATMEGA328BB.bootloader.unlock_bits=0x3F
AVR_ATMEGA328BB.bootloader.lock_bits=0x0F

AVR_ATMEGA328BB.build.mcu=atmega328p
AVR_ATMEGA328BB.build.f_cpu=8000000L
AVR_ATMEGA328BB.build.core=arduino:arduino
AVR_ATMEGA328BB.build.variant=arduino:standard


AVR_ATMEGA328BB.bootloader.tool=arduino:avrdude
AVR_ATMEGA328BB.upload.tool=arduino:avrdude
```
