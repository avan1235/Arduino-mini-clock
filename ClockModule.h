#ifndef CLOCKMODULE_H
#define CLOCKMODULE_H

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define ClockModule_ADDRESS              (0x68)

#define ClockModule_REG_TIME             (0x00)
#define ClockMoodule_REG_ALARM_1          (0x07)
#define ClockModule_REG_ALARM_2          (0x0B)
#define ClockModule_REG_CONTROL          (0x0E)
#define ClockModule_REG_STATUS           (0x0F)
#define ClockModule_REG_TEMPERATURE      (0x11)

#ifndef RTCDATETIME_STRUCT_H
#define RTCDATETIME_STRUCT_H

struct RTCDateTime
{
	uint16_t year;
	uint8_t month;
	uint8_t day;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t dayOfWeek;
	uint32_t unixtime;
};
#endif

typedef enum
{
	ClockModule_1HZ = 0x00,
	ClockModule_4096HZ = 0x01,
	ClockModule_8192HZ = 0x02,
	ClockModule_32768HZ = 0x03
} ClockModule_sqw_t;

typedef enum
{
	ClockModule_EVERY_SECOND = 0b00001111,
	ClockModule_MATCH_S = 0b00001110,
	ClockModule_MATCH_M_S = 0b00001100,
	ClockModule_MATCH_H_M_S = 0b00001000,
	ClockModule_MATCH_DT_H_M_S = 0b00000000,
	ClockModule_MATCH_DY_H_M_S = 0b00010000
} ClockModule_alarm1_t;

typedef enum
{
	ClockModule_EVERY_MINUTE = 0b00001110,
	ClockModule_MATCH_M = 0b00001100,
	ClockModule_MATCH_H_M = 0b00001000,
	ClockModule_MATCH_DT_H_M = 0b00000000,
	ClockModule_MATCH_DY_H_M = 0b00010000
} ClockModule_alarm2_t;

class ClockModule
{
public:

	bool begin(void);

	void setDateTime(uint16_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t minute, uint8_t second);
	void setDateTime(const char* date, const char* time);
	RTCDateTime getDateTime(void);
	uint8_t isReady(void);

	void setBattery(bool timeBattery, bool squareBattery);

	char* dateFormat(const char* dateFormat, RTCDateTime dt);
	uint8_t daysInMonth(uint16_t year, uint8_t month);

private:
	RTCDateTime t;

	char *strDayOfWeek(uint8_t dayOfWeek);
	char *strMonth(uint8_t month);
	char *strAmPm(uint8_t hour, bool uppercase);
	char *strDaySufix(uint8_t day);

	uint8_t hour12(uint8_t hour24);
	uint8_t bcd2dec(uint8_t bcd);
	uint8_t dec2bcd(uint8_t dec);

	long time2long(uint16_t days, uint8_t hours, uint8_t minutes, uint8_t seconds);
	uint16_t date2days(uint16_t year, uint8_t month, uint8_t day);
	uint16_t dayInYear(uint16_t year, uint8_t month, uint8_t day);
	bool isLeapYear(uint16_t year);
	uint8_t dow(uint16_t y, uint8_t m, uint8_t d);

	uint32_t unixtime(void);
	uint8_t conv2d(const char* p);

	void writeRegister8(uint8_t reg, uint8_t value);
	uint8_t readRegister8(uint8_t reg);
};

#endif
