#ifndef ALARM_H_INCLUDED
#define ALARM_H_INCLUDED

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Constants.h"
#include "DrawFunctions.h"

#include <U8g2lib.h>


extern U8G2_SSD1306_128X64_NONAME_1_HW_I2C Display;
extern byte rHeart;
extern bool heartIncrease;

extern unsigned long lastDHT;
extern unsigned long lastHeart;
extern unsigned long lastTwoButtonPressed;
extern unsigned long lastBeep;
extern unsigned long lastLEDs;

extern unsigned long lastButton1Pressed;
extern unsigned long lastButton2Pressed;

extern unsigned long lastAccelerometerRead;

extern byte LEDsState;

extern bool buzzerState;


void beepAlarm();

void checkBothButtons();

void rearmAlarmBeep();

void checkButton1();

void checkButton2();

bool isAlarmTime();

void useLEDs(byte shift);

#endif // ALARM_H_INCLUDED
