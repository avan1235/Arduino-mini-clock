#ifndef ACCELEROMETER_H_INCLUDED
#define ACCELEROMETER_H_INCLUDED

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#define ACCADRR 0x4C

#include <Wire.h>

void beginAccelerometer();

void readAcceleration(float &ax, float &ay, float &az);

#endif // ACCELEROMETER_H_INCLUDED
