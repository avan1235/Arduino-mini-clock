#include "DataToString.h"

String getStringYear() {
    String outTime = "";
    outTime += (String)actTime.year;

    return outTime;
}

String getAlarmState() {
    return ((alarmON == true ? ALARM_ON_STRING : ALARM_OFF_STRING));
}

String getAlarmTime() {
    String out = "";
    if (alarmHour < 10) {
        out += "0" + (String)alarmHour;
    }
    else {
        out += (String)alarmHour;
    }

    out += ":";

    if (alarmMinute < 10) {
        out += "0" + (String)alarmMinute;
    }
    else {
        out += (String)alarmMinute;
    }

    return out;
}

String getStringDayAndMonth() {
    String outTime = "";
    if (actTime.day < 10) {
        outTime += "0" + (String)actTime.day;
    }
    else {
        outTime += (String)actTime.day;
    }
    outTime += ".";

    if (actTime.month < 10) {
        outTime += "0" + (String)actTime.month;
    }
    else {
        outTime += (String)actTime.month;
    }

    return outTime;
}

String getStringTime(bool sec) {
    String outTime = "";

    if (actTime.hour < 10) {
        outTime += "0" + (String)actTime.hour;
    }
    else {
        outTime += (String)actTime.hour;
    }
    outTime += ":";

    if (actTime.minute < 10) {
        outTime += "0" + (String)actTime.minute;
    }
    else {
        outTime += (String)actTime.minute;
    }

    if (sec) {
        outTime += ":";
        if (actTime.second < 10) {
            outTime += "0" + (String)actTime.second;
        }
        else {
            outTime += (String)actTime.second;
        }
    }
    return outTime;
}
