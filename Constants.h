#ifndef CONSTANTS_H_INCLUDED
#define CONSTANTS_H_INCLUDED

#define DHT_PIN  2
#define DHT_DELAY_MILLIS 2000

#define BUTTON_1 4
#define BUTTON_2 3

#define BUZZER_PIN 13
#define BUZZER_TONE 710

#define ACCELEROMETER_INTERVAL_MILLIS 100

#define LED_1 5
#define LED_2 6
#define LED_3 7
#define LED_4 8

#define BUTTONS_WAIT_MILLIS 1000
#define BUTTONS_CHANGE_MILLIS 150

#define M_PI 3.14159265358979323846

#define XSizeH 128
#define YSizeH 64
#define XSizeV 64
#define YSizeV 128

#define DEFAULT_SCREEN_MODE 2
#define DEFAULT_SCREEN_ROTATION U8G2_R2  // have to be set as the DEFAULT_SCREEN_MODE

#define DEFAULT_ALARM_HOUR 6
#define DEFAULT_ALARM_MINUTE 0

#define DEFAULT_LEDS_STATE 0
#define INITIAL_LEDS_STATE 1
#define LEDS_CHANGE_MILLIS 200

#define BEEP_TIME_MILLIS 1000

#define NO_SECONDS 0
#define SECONDS 1

#define SMALL_FONT 1
#define NORMAL_FONT 2
#define BOLD_FONT 3

#define WHITE 1
#define BLACK 0

#define X_ROTATE 1  
#define Y_ROTATE 2
#define Z_ROTATE 3

#define CLOCK_RADIUS 31

#define DOT_SIZE 6
#define NUMBER_SIZE 18
#define NUMBER_BOLD 7
#define VERTICAL 0
#define HORIZONTAL 1

#define ZERO 119
#define ONE 36
#define TWO 93
#define THREE 109
#define FOUR 46
#define FIVE 107
#define SIX 123
#define SEVEN 37
#define EIGHT 127
#define NINE 111

#define THERMOMETER_SIZE 7
#define MIN_TEMP 15
#define MAX_TEMP 35

#define HEART_DELAY 125
#define MIN_R_HEART 2
#define MAX_R_HEART 10

#define BOX_SIZE_X 18
#define BOX_SIZE_Y 9

#define ALARM_ON_STRING "WL."
#define ALARM_OFF_STRING "WYL."

const PROGMEM String daysNames[7] = {"PON", "WT", "SR", "CZW", "PT", "SOB", "NDZ"};
#define LEAP_YEAR(Y)     ( (Y>0) && !(Y%4) && ( (Y%100) || !(Y%400) )) 

#endif // CONSTANTS_H_INCLUDED
