#ifndef DRAWFUNCTIONS_H_INCLUDED
#define DRAWFUNCTIONS_H_INCLUDED

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Constants.h"

#include "DataToString.h"
#include "SensorUses.h"
#include "ClockModule.h"


extern byte XSize;
extern byte YSize;

extern double actTemp, actHum;
extern unsigned long lastHeart;
extern bool heartIncrease;
extern byte rHeart;
extern DS3231 clockToRead;

/*
 * Function to draw the given string using font defined in the constants file
*/

void drawText(String toPrint, byte  x, byte y, byte font);

double angleConvert(byte value, byte div);

void drawCirClock(byte vecX, byte vecY, byte radius);

void drawTemperature(byte x, byte y, byte font);

void drawHumidity(byte x, byte y, byte font);

void drawTime(byte x, byte y, byte font, bool seconds);

void drawModuleOfNumber(byte XVec, byte YVec, bool state);

void drawPartOfNumber(byte XVec, byte YVec, byte whichPart);

void drawRectangle(byte XVec, byte YVec, byte Size);

void drawNumber(byte number, byte positionOfDigit, byte XVec, byte YVec);

void drawDots(byte XVec, byte YVec);

void drawBigTime(byte XVec, byte YVec);

void drawBase(byte XVec, byte YVec);

void drawTermometer(byte XVec, byte YVec);

void drawHumidityPedometer();

void drawSpecialDate(byte XVec, byte YVec, byte font);

void drawHeart(byte XVec, byte YVec, byte R);

void heartAnimation(byte XVec, byte YVec, byte actR);

void drawDaysInCalendar(RTCDateTime time, byte XVec, byte YVec);

void drawCalendar(RTCDateTime time, byte XVec, byte YVec);

void drawAlarmSetting();

void drawNormalScreens();

#endif // DRAWFUNCTIONS_H_INCLUDED
