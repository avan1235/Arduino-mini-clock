#ifndef DATATOSTRING_H_INCLUDED
#define DATATOSTRING_H_INCLUDED

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Constants.h"
#include "ClockModule.h"

extern RTCDateTime actTime;
extern byte alarmHour;
extern byte alarmMinute;
extern bool alarmON;
extern bool alarmBeep;
extern bool settingAlarm;
extern bool setTime;
extern bool alarmButtonClicked;

String getStringYear();

String getAlarmState();

String getAlarmTime();

String getStringDayAndMonth();

String getStringTime(bool sec);

#endif // DATATOSTRING_H_INCLUDED
