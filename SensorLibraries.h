#ifndef SENSORLIBRARIES_H_INCLUDED
#define SENSORLIBRARIES_H_INCLUDED

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Accelerometer.h"
#include <DS3231.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <dht11.h>
#include <math.h>
#include <avr/pgmspace.h>

typedef u8g2_uint_t u8g_uint_t;

#endif // SENSORLIBRARIES_H_INCLUDED
