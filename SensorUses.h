#ifndef SENSORUSES_H_INCLUDED
#define SENSORUSES_H_INCLUDED

#if ARDUINO >= 100
#include "Arduino.h"
#else
#include "WProgram.h"
#endif

#include "Constants.h"
#include "Accelerometer.h"
#include <DS3231.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <dht11.h>

extern float ax, ay, az;
extern U8G2_SSD1306_128X64_NONAME_1_HW_I2C Display;

extern int actScreenMode;
extern bool alarmBeep;
extern bool alarmButtonClicked;

extern byte XSize;
extern byte YSize;

extern RTCDateTime actTime;
extern DS3231 clockToRead;

extern unsigned long lastDHT;

extern int check;
extern dht11 DHT;

extern double actTemp, actHum;


void checkScreenRotation();

void actualizeScreenModeSize();

void actualizeTime();

byte dayOfWeek(uint16_t year, uint8_t month, uint8_t day);

void checkTempAndHum();

#endif // SENSORUSES_H_INCLUDED
