#include "Constants.h"
#include "Accelerometer.h"
#include "SensorUses.h"
#include "DataToString.h"
#include "DrawFunctions.h"
#include "Alarm.h"

#include <DS3231.h>
#include <U8g2lib.h>
#include <Wire.h>
#include <dht11.h>
#include <math.h>
#include <avr/pgmspace.h>

typedef u8g2_uint_t u8g_uint_t;

/**
 * Global variables definition
 */

U8G2_SSD1306_128X64_NONAME_1_HW_I2C Display(U8G2_R0, /* reset=*/ U8X8_PIN_NONE, /* clock=*/ 16, /* data=*/ 17);

dht11 DHT;
int check;

DS3231 clockToRead;
RTCDateTime actTime;

byte alarmHour;
byte alarmMinute;

byte XSize;
byte YSize;

float ax, ay, az;
int actScreenMode;

double actTemp, actHum;

unsigned long lastDHT;
unsigned long lastHeart;
unsigned long lastTwoButtonPressed;
unsigned long lastBeep;
unsigned long lastLEDs;

unsigned long lastButton1Pressed;
unsigned long lastButton2Pressed;

unsigned long lastAccelerometerRead;

byte rHeart;
bool heartIncrease;

bool alarmON;
bool alarmBeep;
bool settingAlarm;
bool setTime;
bool alarmButtonClicked;

byte LEDsState;

bool buzzerState;



void setup() {
	beginAccelerometer();

	pinMode(BUTTON_1, INPUT);
	pinMode(BUTTON_2, INPUT);

	pinMode(LED_1, OUTPUT);
	pinMode(LED_2, OUTPUT);
	pinMode(LED_3, OUTPUT);
	pinMode(LED_4, OUTPUT);

	pinMode(BUZZER_PIN, OUTPUT); // buzzer for alarm

	Display.begin();
	actScreenMode = DEFAULT_SCREEN_MODE;
	Display.setDisplayRotation(DEFAULT_SCREEN_ROTATION);

	clockToRead.begin(); 
	//clockToRead.setDateTime(__DATE__, __TIME__);    // Set time to sketch compiling time
	//clockToRead.setDateTime(2019, 4, 1, 21, 15, 0); // Set time to given in arguments

	lastDHT = millis();
	lastHeart = millis();

	lastButton1Pressed = millis();
	lastButton2Pressed = millis();
	lastTwoButtonPressed = millis();

	lastBeep = millis();
	lastLEDs = millis();

	lastAccelerometerRead = millis();

	rHeart = MIN_R_HEART; // for heart animation during alarm
	heartIncrease = true;

	alarmON = false;
	alarmBeep = false; // beep on alarm time - has to be rearmed
	settingAlarm = false; // setting alarm mode
	alarmButtonClicked = false; // if button to mute alarm has been pushed

	alarmHour = DEFAULT_ALARM_HOUR;
	alarmMinute = DEFAULT_ALARM_MINUTE;

	LEDsState = DEFAULT_LEDS_STATE; // All LEDs turned off

	setTime = false; // setting time or turning alarm on/off
	buzzerState = true; // allow  buzzer beep on alarm - start with sound
}


void loop() {

  checkScreenRotation(); // check if position of accelerometer has changed
  actualizeScreenModeSize(); // depends on screen rotation
  actualizeTime();
  checkTempAndHum();
  checkBothButtons(); // both buttons have to be pushed in the same time for BUTTONS_WAIT_MILLIS
  // LEDsState = 15; // check all LEDs for debug purpose
  useLEDs(LEDsState);

  if (isAlarmTime()) {
    alarmBeep = true;
  }

  if (alarmBeep == true) {
    beepAlarm();
  }
  else if (settingAlarm == true) {
    rearmAlarmBeep();
    drawAlarmSetting();
    checkButton1();
    checkButton2();
  }
  else {
    rearmAlarmBeep();
    drawNormalScreens();
  }
}
